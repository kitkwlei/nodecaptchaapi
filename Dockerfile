FROM node:20

COPY . /opt/nodecaptchaapi
WORKDIR /opt/nodecaptchaapi
ENTRYPOINT ["node", "app.js"]
