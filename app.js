const express = require('express');
const app = express();
const Database = require('better-sqlite3');

app.use(express.static('public'))

app.get('/', (req, res) => res.send('Hello World!'));

var svgCaptcha = require('svg-captcha');
 
app.get('/captcha', function (req, res) {
    var captcha = svgCaptcha.create({ ignoreChars: '1lI0O' });
    console.log(captcha.text);
    var options = {};
	var db = new Database('captcha.db', options);
	var stmt = db.prepare('insert into captcha_store (captcha_text, gen_time_iso8601) values (?,?)');
	var gen_time_iso8601 = (new Date).toISOString();
	var info = stmt.run(captcha.text, gen_time_iso8601);
	captcha.rowid = info.lastInsertRowid;
	db.close();
    res.status(200).send(captcha);
});

app.get('/verifyCaptcha', function (req, res) {
    var options = {};
	var db = new Database('captcha.db', options);
	console.log(req.query.text);
	console.log(req.query.rowid);
	var row = db.prepare('SELECT rowid, captcha_text, gen_time_iso8601 FROM captcha_store where rowid = ?').get(parseInt(req.query.rowid));
	db.close();
	console.log(row);
	if (row != null){
		var gen_time = new Date(row.gen_time_iso8601);
		var current_time = new Date();
		var expireMilliseconds = 900 * 1000; // 1800s = 30m
		if (row.captcha_text === req.query.text){
			if (current_time - gen_time <= expireMilliseconds){
				res.status(200).send({ret:true});
			} else {
				res.status(200).send({ret:false, error:"expired"});
			}
			return;			
		}
	}
	res.status(200).send({ret:false});
});


app.listen(3000, () => console.log('Example app listening on port 3000!'));


